import java.math.BigInteger;
import java.nio.file.Files;
import java.security.InvalidAlgorithmParameterException;
import java.util.Base64;
import javax.crypto.*;
import java.io.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

public class RC4 {
    SecretKey secretKey;

    private void setSecretKey(File file) throws IOException, ClassNotFoundException {
        FileInputStream fin = new FileInputStream(file);
        ObjectInputStream oin = new ObjectInputStream(fin);
        this.secretKey = (SecretKey) oin.readObject();
        oin.close();
    }

    RC4() throws IOException, NoSuchAlgorithmException, NoSuchPaddingException, BadPaddingException, InvalidKeyException, IllegalBlockSizeException, ClassNotFoundException, InvalidAlgorithmParameterException {
        File file;
        Scanner in = new Scanner(System.in);
        String mode = in.next();

        Cipher rc4 = Cipher.getInstance("RC4");

        if (mode.equals("enc")) {
            String path = in.next();
            String key = in.next();
            file = new File(path);
            byte[] fileReader = Files.readAllBytes(file.toPath());
            setSecretKey(new File(key));
            byte[] cipherText = encrypt(fileReader, secretKey, rc4);
            FileOutputStream stream = new FileOutputStream(new File("out"));
            stream.write(Base64.getEncoder().encode(cipherText));
            stream.close();


        } else if (mode.equals("dec")) {
            String path = in.next();
            String key = in.next();
            file = new File(path);
            byte[] fileReader = Base64.getDecoder().decode(Files.readAllBytes(file.toPath()));
            setSecretKey(new File(key));
            byte[] plainText = decrypt(fileReader, secretKey, rc4);
            String plaintextBack = new String(plainText);
            System.out.println(plaintextBack);
        } else if (mode.equals("key")) {
            KeyGenerator keyGenerator = KeyGenerator.getInstance("RC4");
            this.secretKey = keyGenerator.generateKey();
            FileOutputStream stream = new FileOutputStream(new File("key"));
            stream.write(toHex(Base64.getEncoder().encodeToString(secretKey.getEncoded())));
            stream.close();
            stream = new FileOutputStream(new File("keyObject"));
            ObjectOutputStream oos = new ObjectOutputStream(stream);
            oos.writeObject(secretKey);
            oos.close();
            stream.close();
        }
    }

    public byte[] toHex(String arg) throws UnsupportedEncodingException {
        return String.format("%x", new BigInteger(1, arg.getBytes("UTF-8"))).getBytes();
    }

    private byte[] encrypt(byte[] plainText, SecretKey secretKey, Cipher rc4) throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        rc4.init(Cipher.ENCRYPT_MODE,secretKey);
        byte[] cipherText = rc4.doFinal(plainText);
        Base64.getEncoder().encode(cipherText);
        return cipherText;
    }

    private byte[] decrypt(byte[] cipherText, SecretKey secretKey, Cipher rc4) throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException, InvalidAlgorithmParameterException {
        rc4.init(Cipher.DECRYPT_MODE,secretKey,rc4.getParameters());
        byte[] plainText = rc4.doFinal(cipherText);
        Base64.getEncoder().encode(cipherText);
        return plainText;
    }

    public static void main(String[] args) throws NoSuchPaddingException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, IOException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException, ClassNotFoundException {
        new RC4();
    }

}

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.Mac;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.security.Security;

public class AES {


    private byte[] keyBytes = new byte[]{0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09,
        0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17};

    public byte[] call(String message, boolean server) throws Exception {
        //Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
        byte[] input = message.getBytes();

        byte[] ivBytes = new byte[]{0x00, 0x01, 0x02, 0x03, 0x00, 0x01, 0x02, 0x03, 0x00, 0x00, 0x00,
                0x00, 0x00, 0x00, 0x00, 0x01};


        SecretKeySpec key = new SecretKeySpec(keyBytes, "AES");
        IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);
        Cipher cipher = Cipher.getInstance("AES/CTR/nopadding", "BC");
        //System.out.println("input : " + new String(input));
        byte[] cipherText;
        if (!server) {
            // encryption pass
            cipher.init(Cipher.ENCRYPT_MODE, key, ivSpec);
            ByteArrayInputStream bIn = new ByteArrayInputStream(input);
            CipherInputStream cIn = new CipherInputStream(bIn, cipher);
            ByteArrayOutputStream bOut = new ByteArrayOutputStream();

            int ch;
            while ((ch = cIn.read()) >= 0) {
                bOut.write(ch);
            }

            cipherText = bOut.toByteArray();
            //System.out.println("cipher: " + new String(cipherText));
        } else {
            // decryption pass
            cipher.init(Cipher.DECRYPT_MODE, key, ivSpec);
            ByteArrayOutputStream bOut = new ByteArrayOutputStream();
            CipherOutputStream cOut = new CipherOutputStream(bOut, cipher);
            cOut.write(input);
            cOut.close();
            cipherText = bOut.toByteArray();
            //System.out.println("plain : " + new String(bOut.toByteArray()));
        }
        return cipherText;
    }

    public byte[] sign(byte[] message) throws Exception{
        SecretKeySpec skeySpec = new SecretKeySpec(keyBytes, "HmacSHA256");
        Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
        sha256_HMAC.init(skeySpec);
        return sha256_HMAC.doFinal(message);
    }



    public static void main(String[] args) throws Exception {
        AES o = new AES();
        String i = new String(o.call("teste",false));
        String e = new String(o.call(i,true));
        System.out.println(i +"\n" + e);
    }
}

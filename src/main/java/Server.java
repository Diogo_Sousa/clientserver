import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server{
    ServerSocket serverSocket;
    Socket socket;

    int client=0;

    public Server(){
        try {
            serverSocket = new ServerSocket(8003);
            while(true){
                socket = serverSocket.accept();
                new ConnectionHandler(socket,client++).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        new Server();
    }
}

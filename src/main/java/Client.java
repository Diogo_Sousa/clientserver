import org.bouncycastle.util.encoders.Base64;

import java.io.*;
import java.net.*;
import java.util.Scanner;
import java.util.concurrent.Callable;

public class Client extends Thread{
    Thread inputThread;
    Thread outputThread;
    BufferedReader b1,b2;
    Socket socket;
    PrintWriter ptr;
    String msgIn="",msgOut="";

    public Client() {
        try {
            inputThread = new Thread(this);
            outputThread = new Thread(this);
            socket = new Socket("localhost", 8003);
            inputThread.start();;
            outputThread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void run() {
        try{
            if(Thread.currentThread()==inputThread){
                AES aes = new AES();
                do {
                    b1 = new BufferedReader(new InputStreamReader(System.in));
                    ptr = new PrintWriter(socket.getOutputStream(), true);
                    msgIn = b1.readLine();
                    msgIn = new String((aes.call(msgIn, false)));

                    ptr.println(msgIn);
                    msgIn = new String(aes.sign(msgIn.getBytes()));
                    ptr.println(msgIn);
                    System.out.println(msgIn);
                }while(!msgIn.equals("exit"));
            }else{
                do{
                    b2 = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    msgOut = b2.readLine();
                    System.out.println(msgOut);
                }while(!msgOut.equals("exit"));
            }
            return;
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new Client();
    }

}
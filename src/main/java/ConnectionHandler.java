import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ConnectionHandler extends Thread{

    BufferedReader b2;
    PrintWriter ptr;
    Socket socket;
    int cliente;
    PrintWriter ptrint;
    String msgIn="";
    String msgOut="";
    BufferedReader b1;

    public ConnectionHandler(Socket socket,int client){
        this.socket = socket;
        this.cliente = client;
    }

    public void run() {
        try {
            System.out.println(cliente);
            AES aes = new AES();
            do {
                b2 = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                msgOut = b2.readLine();
                String check = new String(aes.sign(msgOut.getBytes()));
                System.out.println(check);
                msgOut = new String (aes.call(msgOut,true));
                System.out.println("["+cliente+"]: "+msgOut);
                msgOut = b2.readLine();
                if(check.equals(msgOut)) System.out.print("AUTENTICADO");
                System.out.println("["+cliente+"]: HASH: "+msgOut);
            }while(!msgOut.equals("exit"));
            //ptr = new PrintWriter(socket.getOutputStream(),true);
            //ptr.println("=["+cliente+"]=");
            System.out.println("=["+cliente+"]=");
        }
        catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}